const CREATE_USER_TABLE = `
    CREATE TABLE IF NOT EXISTS users (
        id int(11) NOT NULL AUTO_INCREMENT,
        name varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
        gender varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
        email varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
        dob varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
        phone varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
        cell varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
        address varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
        picture_thumbnail varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
        picture_medium varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
        picture_large varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
        active int(11) DEFAULT '1',
        createdAt timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        updatedAt timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (id),
        UNIQUE KEY id_UNIQUE (id)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
      
`;

module.exports.CREATE_USER_TABLE = CREATE_USER_TABLE;
