const SELECT_ALL_USERS = () => {
  return `
        SELECT * FROM users
            WHERE active = 1;
    `;
};

const SELECT_USER_BY_ID = (id) => {
  return `
        SELECT * FROM users
            WHERE id = ${id} 
            AND active = 1;
    `;
};

module.exports.SELECT_ALL_USERS = SELECT_ALL_USERS;
module.exports.SELECT_USER_BY_ID = SELECT_USER_BY_ID;
