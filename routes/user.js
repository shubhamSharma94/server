const createUser = require("../api/user/createUser");
const getUser = require("../api/user/getUser");
const deleteUser = require("../api/user/deleteUser");

const routes = [createUser, getUser, deleteUser];

module.exports = (app) => {
  routes.forEach((func) => {
    func(app);
  });
};
