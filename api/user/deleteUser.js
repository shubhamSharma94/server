const { deleteAllUser, deleteUserById } = require("../../utils/userDbActions");
module.exports = (app) => {
  app.delete("/user/:id", async (req, res) => {
    const id = req.params.id;
    const isUserDeleted = await deleteUserById(id);
    if (isUserDeleted) {
      return res.sendStatus(204);
    }
    res.sendStatus(500);
  });

  app.delete("/user", async (req, res) => {
    const isAllUserDeleted = await deleteAllUser();
    if (isAllUserDeleted) {
      return res.sendStatus(204);
    }
    res.sendStatus(500);
  });
};
