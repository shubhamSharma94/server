const { selectAllUser } = require("../../utils/userDbActions");

module.exports = (app) => {
  app.get("/user", async (req, res) => {
    const allUsers = await selectAllUser();
    res.status(201).json(allUsers);
  });
};
