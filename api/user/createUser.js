const { createUser } = require("../../utils/createUser");
const { insertUser, selectUserById } = require("../../utils/userDbActions");
const { buildUserObject } = require("../../utils/utils");

module.exports = (app) => {
  app.post("/user", async (req, res) => {
    const newUser = await createUser();
    const newUserObj = await buildUserObject(newUser);
    const isUserCreated = await insertUser(newUserObj);
    let createdUser;
    if (isUserCreated) {
      createdUser = await selectUserById(isUserCreated);
      return res.status(201).json(createdUser);
    }
    res.status(500);
  });
};
