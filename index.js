const express = require("express");
const bodyParser = require("body-parser");
const connection = require("./database/dbConnection");
const cors = require("cors");
const path = require("path");

const port = process.env.PORT || 4000;

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.use("/", express.static(path.resolve(__dirname, "static")));

const routes = require("./routes")(app);

let server = app.listen(port, () => {
  console.log(`Listening on port ${server.address().port}`);
});

process.on("uncaughtException", function (err) {
  console.log("Caught exception: ", err);
});
