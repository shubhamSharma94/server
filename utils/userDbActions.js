const { INSERT_USER } = require("../database/userQueries/insertUserQuery");
const { connection } = require("../database/dbConnection");
const {
  SELECT_USER_BY_ID,
  SELECT_ALL_USERS,
} = require("../database/userQueries/selectUserQueries");
const {
  DELETE_ALL_USER,
  DELETE_USER_BY_ID,
} = require("../database/userQueries/deleteUserQuery");

const insertUser = (userObj) => {
  return new Promise((resolve, reject) => {
    connection.query(INSERT_USER(userObj), async (error, results, fields) => {
      if (error) return reject(false);
      resolve(results.insertId);
    });
  });
};

const selectUserById = (id) => {
  return new Promise((resolve, reject) => {
    let userId = connection.escape(id); // for escaping SQL injections
    connection.query(
      SELECT_USER_BY_ID(userId),
      async (error, results, fields) => {
        if (error) return reject(false);
        resolve(results);
      }
    );
  });
};

const selectAllUser = () => {
  return new Promise((resolve, reject) => {
    connection.query(SELECT_ALL_USERS(), async (error, results, fields) => {
      if (error) return reject(false);
      resolve(results);
    });
  });
};

const deleteAllUser = () => {
  return new Promise((resolve, reject) => {
    connection.query(DELETE_ALL_USER(), async (error, results, fields) => {
      if (error) return reject(false);
      resolve(true);
    });
  });
};

const deleteUserById = (id) => {
  return new Promise((resolve, reject) => {
    let userId = connection.escape(id); // for escaping SQL injections
    connection.query(
      DELETE_USER_BY_ID(userId),
      async (error, results, fields) => {
        if (error) return reject(false);
        resolve(true);
      }
    );
  });
};

module.exports.insertUser = insertUser;
module.exports.selectUserById = selectUserById;
module.exports.selectAllUser = selectAllUser;
module.exports.deleteAllUser = deleteAllUser;
module.exports.deleteUserById = deleteUserById;
