# Node Server

This is a Node api server.

### How do I get set up?

- git clone https://bitbucket.org/shubhamSharma94/server.git
- cd server
- npm install
- insert .env file for DB connection

  - DB_HOST: Please spesify host URL
  - DB_USER: Please spesify DB username
  - DB_PASSWORD: Please spesify DB password
  - DB_SCHEMA: Please spesify DatabaseName

- npm run dev // for development mode
- npm start

### Who do I talk to?

- Repo owner
